package test.harness

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import groovyx.net.http.RESTClient


class EarthquakeController {

    def index() {
        def ret = null
        def baseUrl = 'https://httpbin.org/'
        def path = '/encoding/utf8'
        def http = new HTTPBuilder(baseUrl)

        // perform a POST request, expecting TEXT response
        http.request(Method.GET, ContentType.TEXT) {
            uri.path = path
            headers.'User-Agent' = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'

            // response handler for a success response code
            response.success = { resp, reader ->
                println "response status: ${resp.statusLine}"
                println 'Headers: -----------'
                resp.headers.each { h ->
                    println " ${h.name} : ${h.value}"
                }

                ret = reader.getText()

                println 'Response data: -----'
                println ret
                println '--------------------'
            }
        }
        return ret

    }
}
